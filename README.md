# KiCad Nightly Footprints Library Flatpak Extension Manifest

**Caution:** *This is work in progress. You can use the manifest to build, and
install the kicad-nightly-flatpak-extension-footprints library extension.*


## Introduction

Technically, this is a flatpak manifest to build the tip of the kicad-footprints
library `master` branch. It will become a nightly flatpak extension when the
nightly automated builds are set up, and the build results will been uploaded to
a kicad-nightly flatpak repo for interested people to consume.


## Build, and install locally

### Prerequisites

First you need to build and install the
[KiCad Nightly flatpak](https://gitlab.com/kicad/packaging/kicad-flatpak/kicad-nightly-flatpaks/kicad-nightly-flatpak).


### Clone, build, and install kicad-nightly-flatpak-extension-footprints locally

```console
$ git clone https://gitlab.com/kicad/packaging/kicad-flatpak/kicad-nightly-flatpaks/kicad-nightly-flatpak-extension-footprints
$ cd kicad-nightly-flatpak-extension-footprints
$ mkdir builddir
$ flatpak-builder --user --install ./builddir org.kicad.KiCad.Nightly.Library.Footprints.yml
```

### Subsequent rebuilds

Rebuild as follows:

```console
$ rm -rf builddir/*
$ flatpak-builder --user --install ./builddir org.kicad.KiCad.Nightly.Library.Footprints.yml
```


## Run

After installing, you can run the KiCad Nightly flatpak as usual, and it will
load your newly built footprints:

```console
$ flatpak run --user org.kicad.KiCad.Nightly
```
